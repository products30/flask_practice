from flask import Flask, redirect, url_for, render_template

app = Flask(__name__)

@app.route("/<page>")
def home(page):
    return render_template("index.html", content_page=page, content_names=["tim", "bill", "jobs"])

if __name__ == "__main__":
    app.run()